using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ClickManager : MonoBehaviour
{
    // Start is called before the first frame update
    public delegate void Click();
    public event Click onclick;

    public void clicked()
    {
        onclick?.Invoke();
        onclick = null;
    }
}
