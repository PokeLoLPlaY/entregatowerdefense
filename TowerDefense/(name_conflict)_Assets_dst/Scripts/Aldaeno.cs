using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Aldaeno : MonoBehaviour
{
    public bool selected = false;
    public float speed = 10f;
    bool mov=false;
    
    Vector2 lastclickedpos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (selected&&Input.GetMouseButtonDown(1)) 
        {
            lastclickedpos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mov = true;
        }
        if (mov&&(Vector2)transform.position!=lastclickedpos)
        {
            float step=speed*Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, lastclickedpos, step);
        }
        else
        {
            mov = false;
        }




    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag=="Arbol") 
        {
            
            this.mov = false;
        }
    }
    private void OnMouseDown()
    {
        selected = true;
    }
    


}
