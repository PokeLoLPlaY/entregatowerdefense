using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arbol : MonoBehaviour
{
    public delegate void Contact();
    public event Contact OnContact;
    public int Nresources = 1000;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Nresources <=0) 
        {
            Destroy(this.gameObject);
        }
    }
}
