using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickManager : MonoBehaviour
{
    public delegate void click();
    public event click clickevent;
    // Start is called before the first frame update
    void clicked()
    {
        clickevent?.Invoke();
        clickevent = null;
    }
}
