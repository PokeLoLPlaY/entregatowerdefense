using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    public GameObject focus;
    public int cd;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(disparar());
    }

    // Update is called once per frame
    void Update()
    {
       this.focus = this.GetComponentInParent<Torre>().enemigo;
    }

    IEnumerator disparar()
    {
        while (true)
        {
            yield return new WaitForSeconds(cd);

            if (this.focus != null)
            {
                for (int i = 0; i < this.transform.childCount; i++)
                {
                    if (!this.transform.GetChild(i).gameObject.activeSelf)
                    {
                        this.transform.GetChild(i).gameObject.SetActive(true);
                        this.transform.GetChild(i).position = this.transform.position;
                        print(this.transform.GetChild(i));
                        break;
                    }
                }

            }

        }
        
    }
}
