using System.Collections;
using System.Collections.Generic;
using UnityEditor.Search;
using UnityEngine;

public class ArmaTorre : MonoBehaviour
{
    public Sprite ballesta;

    public Sprite bola;

    public Sprite mortero;

    public GameObject focus;

    public bool ajuste;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponentInParent<NewTorre>().datos.tipo == TorreScriptable.Tipo.Arquero)
        {
            this.GetComponent<SpriteRenderer>().sprite = ballesta;
        }
        else if (GetComponentInParent<NewTorre>().datos.tipo == TorreScriptable.Tipo.Mago)
        {
            if (!ajuste)
            {
                transform.position = new Vector3(GetComponentInParent<Transform>().position.x, GetComponentInParent<Transform>().position.y + 0.177f, GetComponentInParent<Transform>().position.z);
                ajuste = true;
            }
            this.GetComponent<SpriteRenderer>().sprite = bola;
        }
        else if (GetComponentInParent<NewTorre>().datos.tipo == TorreScriptable.Tipo.Mortero)
        {
            this.GetComponent<SpriteRenderer>().sprite = mortero;
        }

        if (GetComponentInParent<NewTorre>().focus != null)
        {
            this.focus = GetComponentInParent<NewTorre>().focus;
            float ikerx = this.transform.position.x - this.focus.transform.position.x;
            float ikery = this.transform.position.y - this.focus.transform.position.y;

            if (GetComponentInParent<NewTorre>().datos.tipo == TorreScriptable.Tipo.Arquero || GetComponentInParent<NewTorre>().datos.tipo == TorreScriptable.Tipo.Mortero)
            {
                float anglerad = Mathf.Atan2(ikery, ikerx);
                float angle = Mathf.Rad2Deg * anglerad;
                this.transform.rotation = Quaternion.Euler(0, 0, angle + 90);
            }
        }
        
    }
}
