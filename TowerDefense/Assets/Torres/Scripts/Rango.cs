using System.Collections;
using System.Collections.Generic;
using UnityEditor.Search;
using UnityEngine;

public class Rango : MonoBehaviour
{
    public Queue<GameObject> cola = new Queue<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float x = GetComponentInParent<NewTorre>().datos.Rango;

        this.transform.localScale = new Vector3(x, x, x);

        if (cola.Count != 0)
        {
            GetComponentInParent<NewTorre>().focus = cola.Peek();
        }
        else
        {
            GetComponentInParent<NewTorre>().focus = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemigos")
        {
            cola.Enqueue(collision.gameObject);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemigos")
        {
            Queue<GameObject> cola2 = new Queue<GameObject>();

            foreach (GameObject obj in cola)
            {
                if (!obj.Equals(collision.gameObject))
                {
                    cola2.Enqueue(obj);
                }
            }

            cola = cola2;

            GetComponentInParent<NewTorre>().focus = null;

        }
    }

}
