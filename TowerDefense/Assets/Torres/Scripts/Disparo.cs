using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    public int dmg;
    public GameObject focus;
    public TipoUnidad tipo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        float ikerx = this.transform.position.x - this.focus.transform.position.x;
        float ikery = this.transform.position.y - this.focus.transform.position.y;

        float valorNormalizacion;
        if (Mathf.Abs(ikerx) > Mathf.Abs(ikery))
            valorNormalizacion = Mathf.Abs(ikerx);
        else
            valorNormalizacion = Mathf.Abs(ikery);

        if (this.transform.tag == "Flecha")
        {
            float anglerad = Mathf.Atan2(ikery, ikerx);
            float angle = Mathf.Rad2Deg * anglerad;
            this.transform.rotation = Quaternion.Euler(0, 0, angle + 90);
            this.GetComponent<Rigidbody2D>().velocity = transform.up * 8;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (this.focus == null)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            float ikerx = this.transform.position.x - this.focus.transform.position.x;
            float ikery = this.transform.position.y - this.focus.transform.position.y;

            float valorNormalizacion;
            if (Mathf.Abs(ikerx) > Mathf.Abs(ikery))
                valorNormalizacion = Mathf.Abs(ikerx);
            else
                valorNormalizacion = Mathf.Abs(ikery);

            if (this.transform.tag == "Flecha")
            {
                float anglerad = Mathf.Atan2(ikery, ikerx);
                float angle = Mathf.Rad2Deg * anglerad;
                this.transform.rotation = Quaternion.Euler(0, 0, angle + 90);
                this.GetComponent<Rigidbody2D>().velocity = transform.up * 8;
            }
            else if (this.transform.tag == "Energia")
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(ikerx / valorNormalizacion * -6, ikery / valorNormalizacion * -6);
                this.transform.Rotate(0, 0, 1);
            }
            else if (this.transform.tag == "Mortero")
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(ikerx / valorNormalizacion * -4, ikery / valorNormalizacion * -4);
                this.transform.Rotate(0, 0, 0.3f);
            }else if (this.transform.tag == "Melee")
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(ikerx / valorNormalizacion * -10, ikery / valorNormalizacion * -10);
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (this.tipo == TipoUnidad.Aliado)
        {
            if (this.transform.parent.transform.tag == "Mortero")
            {
                if (collision.gameObject == this.focus)
                {
                    collision.GetComponent<Unidad>().da�ar(this.dmg);
                    this.focus = null;
                    this.gameObject.SetActive(false);
                }
            }
            else
            {
                if (collision.transform.tag == "Enemigos")
                {
                    collision.GetComponent<Unidad>().da�ar(this.dmg);
                    this.focus = null;
                    this.gameObject.SetActive(false);
                }
            }
            
        }else if (this.tipo == TipoUnidad.Enemigo)
        {
            if (collision.transform.tag == "Aliados" || collision.transform.tag == "Almacen") 
            {
                if (collision.GetComponent<Unidad>() != null)
                {
                    collision.GetComponent<Unidad>().da�ar(this.dmg);
                    this.focus = null;
                    this.gameObject.SetActive(false);
                }
                else if (collision.GetComponent<NewTorre>() != null)
                {
                    collision.GetComponent<NewTorre>().da�ar(this.dmg);
                    this.focus = null;
                    this.gameObject.SetActive(false);
                }
                else if (collision.GetComponent<Almacen>() != null)
                {
                    collision.GetComponent<Almacen>().da�ar(this.dmg);
                    this.focus = null;
                    this.gameObject.SetActive(false);
                }
                else if (collision.GetComponent<EdificiosFunc>() != null)
                {
                    collision.GetComponent<EdificiosFunc>().da�ar(this.dmg);
                    this.focus = null;
                    this.gameObject.SetActive(false);
                }

            }
        }
        
        
    }

}
