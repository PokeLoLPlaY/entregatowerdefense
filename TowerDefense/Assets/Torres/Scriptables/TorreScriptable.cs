using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TorreScriptable : ScriptableObject
{
    public Sprite sprite;

    public string nombre;

    public string info;
    public enum Tipo { Arquero, Mago, Mortero };

    public Tipo tipo;

    public float cd;

    public float Rango;

    public Coste costeResources;

}
