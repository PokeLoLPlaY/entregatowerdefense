using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject resourcesContainer;

    private CanvasRenderer[] allchildren;

    public int comida = 500;
    public int oro = 100;
    public int madera = 250;
    public int piedra = 200;

    private CanvasRenderer cont_comida;
    private CanvasRenderer cont_oro;
    private CanvasRenderer cont_madera;
    private CanvasRenderer cont_piedra;

    public GameObject[] menuItemsToClose;
    
    public void Awake()
    {
        allchildren = resourcesContainer.transform.GetComponentsInChildren<CanvasRenderer>();

        cont_comida = allchildren[5];

        cont_oro = allchildren[6];

        cont_madera = allchildren[7];

        cont_piedra = allchildren[8];
    }
    // Start is called before the first frame update
    void Start()
    {
        /*
        for (int i = 0; i < allchildren.Length; i++)
        {
            print("Hijo " + i + ", Nombre: " + allchildren[i].transform.name);
        }
        */
        StartCoroutine(refreshscan());

    }

    // Update is called once per frame
    void Update()
    {
        cont_comida.GetComponent<TextMeshProUGUI>().text = comida.ToString();
        cont_oro.GetComponent<TextMeshProUGUI>().text = oro.ToString();
        cont_madera.GetComponent<TextMeshProUGUI>().text = madera.ToString();
        cont_piedra.GetComponent<TextMeshProUGUI>().text = piedra.ToString();
    }

    public void closeMainMenu()
    {
        for (int i = 0;i < menuItemsToClose.Length;i++)
        {
            menuItemsToClose[i].SetActive(false);
        }
    }

    public void gastarRecursos(int comida, int oro, int madera, int piedra)
    {
        this.comida -= comida;
        this.oro -= oro;
        this.madera -= madera;
        this.piedra -= piedra;
    }

    IEnumerator refreshscan()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);

            var graph = AstarPath.active.data.gridGraph;
            AstarPath.active.Scan(graph);

        }
        

    }
}
