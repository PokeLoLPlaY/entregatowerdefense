using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class Mov : MonoBehaviour
{
    public bool selected = false;
    public ClickManager clickManager;
    Vector2 lastclickedpos;
    IAstarAI ai;
    public bool mov = false;
    public float speed = 10f;
    bool noPos = true;
    public GameObject focusmov;
    public bool ontarget;
    //pito
    private void OnEnable()
    {
        ai = GetComponent<IAstarAI>();
        if (ai != null) ai.onSearchPath += Update;
    }

    void OnDisable()
    {
        if (ai != null) ai.onSearchPath -= Update;
    }
    // Start is called before the first frame update
    void Start()
    {
        clickManager = FindObjectOfType<ClickManager>();
        ai.destination = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        

        if (selected)
        {
            this.GetComponent<SpriteRenderer>().color = new Color(255, 120, 120, 1f);
        }

        //Select the spected position
        if (selected && Input.GetMouseButtonDown(1))
        {
            lastclickedpos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mov = true;
            noPos = false;
            ai.isStopped = false;
        }
        else if (this.GetComponent<Unidad>().focus != null)
        {
            ontarget = true;
            ai.isStopped = true;
            mov = false;
        }
        else if (this.GetComponentInParent<Unidad>().focusmov != null)
        {
            this.focusmov = this.GetComponentInParent<Unidad>().focusmov;
            lastclickedpos = this.focusmov.transform.position;
            mov = true;
            noPos = false;
            ai.isStopped = false;
        }

        if (!noPos && ai != null) ai.destination = lastclickedpos;

        //Move to the seleted position
        if (mov && (Vector2)transform.position != lastclickedpos)
        {
            /*
            float step = speed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, lastclickedpos, step);
            */
        }
        //Stop once he get the selected position
        else
        {
            mov = false;
        }

        if (lastclickedpos != null && mov)
        {
            if (lastclickedpos.x < this.transform.position.x)
            {
                this.GetComponent<SpriteRenderer>().flipX = true;
            }
            else if (lastclickedpos.x > this.transform.position.x)
            {
                this.GetComponent<SpriteRenderer>().flipX = false;
            }
        }
        
    }
    /*
    public void arreglarZ()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0);
    }
    */

    private void OnMouseDown()
    {

        
        clickManager.GetComponent<ClickManager>().clicked();
        clickManager.GetComponent<ClickManager>().clicobject = this.gameObject;

        selected = true;
        print("Entro");
        clickManager.GetComponent<ClickManager>().onclick += unclick;

    }
    //deselect the Entity
    public void unclick()
    {
        clickManager.clicobject = null;
        selected = false;
        

    }
}
