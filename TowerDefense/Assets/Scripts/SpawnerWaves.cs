using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static TorreScriptable;

public class SpawnerWaves : MonoBehaviour
{
    public int numwave = -1;
    public int cd;
    public GameObject escorpion;//-95x
    public GameObject fantasmon;//-100x y=-20/20
    public GameObject calvo;//-95x
    public GameObject magomalo;//-100x
    public GameObject gigantemalo;//-90x 
    public List<GameObject> vivos;
    public bool waveactive = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(oleada());
    }

    // Update is called once per frame
    void Update()
    {
        if (vivos.Count == 0)
        {
            this.waveactive = false;
        }
    }

    IEnumerator oleada()
    {
        while (true)
        {
            if (this.waveactive == false)
            {
                yield return new WaitForSeconds(cd);

                //print(this.transform.tag+" "+this.cd);

                this.waveactive = true;

                if (this.numwave < 5)
                {
                    for (int i = 0; i < numwave * 2; i++)
                    {
                        GameObject esc = Instantiate(escorpion);
                        esc.transform.position = new Vector2(-95, Random.Range(-20, 20));
                        vivos.Add(esc);

                        int x = Random.Range(0, 5 - numwave);

                        if (x == 0)
                        {
                            GameObject clv = Instantiate(calvo);
                            calvo.transform.position = new Vector2(-95, Random.Range(-20, 20));
                            vivos.Add(clv);
                        }

                    }

                    for (int i = 0; i < numwave; i++)
                    {
                        GameObject fnt = Instantiate(fantasmon);
                        fnt.transform.position = new Vector2(-100, Random.Range(-20, 20));
                        vivos.Add(fnt);
                    }

                }
                else if (this.numwave < 10)
                {
                    for (int i = 0; i < numwave * 3; i++)
                    {
                        GameObject esc = Instantiate(escorpion);
                        esc.transform.position = new Vector2(-95, Random.Range(-20, 20));
                        vivos.Add(esc);

                        int x = Random.Range(0, 30 - numwave);

                        if (x == 0)
                        {
                            GameObject gnt = Instantiate(gigantemalo);
                            gnt.transform.position = new Vector2(-90, Random.Range(-20, 20));
                            vivos.Add(gnt);
                        }

                    }

                    for (int i = 0; i < numwave; i++)
                    {
                        GameObject fnt = Instantiate(fantasmon);
                        fnt.transform.position = new Vector2(-100, Random.Range(-20, 20));
                        vivos.Add(fnt);
                    }

                    for (int i = 0; i < numwave/2; i++)
                    {
                        GameObject clv = Instantiate(calvo);
                        clv.transform.position = new Vector2(-95, Random.Range(-20, 20));
                        vivos.Add(clv);
                    }

                    for (int i = 0; i < numwave / 2; i++)
                    {
                        GameObject mg = Instantiate(magomalo);
                        mg.transform.position = new Vector2(-100, Random.Range(-20, 20));
                        vivos.Add(mg);
                    }

                }
                else if (this.numwave == 10)
                {
                    for (int i = 0; i < numwave * 3; i++)
                    {
                        GameObject esc = Instantiate(escorpion);
                        esc.transform.position = new Vector2(-95, Random.Range(-20, 20));
                        vivos.Add(esc);
                    }

                    for (int i = 0; i < numwave * 2; i++)
                    {
                        GameObject fnt = Instantiate(fantasmon);
                        fnt.transform.position = new Vector2(-100, Random.Range(-20, 20));
                        vivos.Add(fnt);
                    }

                    for (int i = 0; i < numwave; i++)
                    {
                        GameObject clv = Instantiate(calvo);
                        clv.transform.position = new Vector2(-95, Random.Range(-20, 20));
                        vivos.Add(clv);
                    }

                    for (int i = 0; i < numwave; i++)
                    {
                        GameObject mg = Instantiate(magomalo);
                        mg.transform.position = new Vector2(-100, Random.Range(-20, 20));
                        vivos.Add(mg);
                    }

                    for (int i = 0; i < 5; i++)
                    {
                        GameObject gnt = Instantiate(gigantemalo);
                        gnt.transform.position = new Vector2(-90, Random.Range(-20, 20));
                        vivos.Add(gnt);
                    }

                }

                this.numwave++;

            }

            else
            {
                yield return new WaitForSeconds(0);
            }
            
           

        }

    }

}
