using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class menuShow : MonoBehaviour
{
    public bool hidden = true;

    public GameObject backgroundBuilding;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void moveMenu()
    {
        if (hidden)
        {
            backgroundBuilding.GetComponent<RectTransform>().position = new Vector2(backgroundBuilding.GetComponent<RectTransform>().position.x-75, backgroundBuilding.GetComponent<RectTransform>().position.y);
            
        } else
        {
            backgroundBuilding.GetComponent<RectTransform>().position = new Vector2(backgroundBuilding.GetComponent<RectTransform>().position.x+75, backgroundBuilding.GetComponent<RectTransform>().position.y);
        }
        hidden = !hidden;

    }
}
