using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewTorre : MonoBehaviour
{
    public TorreScriptable datos;
    public Sprite arquero;
    public Sprite mago;
    public Sprite mortero;
    public GameObject pool;
    public GameObject focus;
    public int maxhp;
    private int hp;

    // Start is called before the first frame update
    void Start()
    {
        this.hp = this.maxhp;
        if (this.datos.tipo == TorreScriptable.Tipo.Arquero)
        {
            this.GetComponent<SpriteRenderer>().sprite = arquero;
            pool = GameObject.Find("PoolArqueroGenerica");
        }
        else if (this.datos.tipo == TorreScriptable.Tipo.Mago)
        {
            this.GetComponent<SpriteRenderer>().sprite = mago;
            pool = GameObject.Find("PoolMagoGenerica");
        }
        else if (this.datos.tipo == TorreScriptable.Tipo.Mortero)
        {
            this.GetComponent<SpriteRenderer>().sprite = mortero;
            pool = GameObject.Find("PoolMorteroGenerica");
        }

        StartCoroutine(disparar());

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void da�ar(int dmg)
    {
        this.hp = this.hp - dmg;
        if (this.hp <= 0)
        {
            this.morir();
        }
    }
    public void morir()
    {
        Destroy(this.gameObject);
    }

    IEnumerator disparar()
    {
        while (true)
        {
            yield return new WaitForSeconds(this.datos.cd);

            if (this.focus != null)
            {
                for (int i = 0; i < this.pool.transform.childCount; i++)
                {
                    if (!this.pool.transform.GetChild(i).gameObject.activeSelf)
                    {
                        this.pool.transform.GetChild(i).GetComponent<Disparo>().tipo = TipoUnidad.Aliado;
                        this.pool.transform.GetChild(i).GetComponent<Disparo>().focus = this.focus;
                        this.pool.transform.GetChild(i).gameObject.SetActive(true);
                        this.pool.transform.GetChild(i).position = this.transform.position;
                        break;
                    }
                }

            }

        }
    }

}
