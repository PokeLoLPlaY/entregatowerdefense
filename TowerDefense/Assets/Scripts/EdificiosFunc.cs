using Mono.Cecil;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EdificiosFunc : MonoBehaviour
{

    ClickManager clickManager;

    public Edificios edificio;

    private int hpmax;

    private int hp;

    public bool selected = false;

    public TipoEdf edf;

    // Start is called before the first frame update
    void Start()
    {
        this.hp = edificio.health;
        this.hpmax = edificio.health;
        this.GetComponent<SpriteRenderer>().sprite = edificio.sprite;
        this.edf = edificio.edf;
        clickManager = FindObjectOfType<ClickManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnMouseDown()
    {

        clickManager.GetComponent<ClickManager>().clicked();

        clickManager.GetComponent<ClickManager>().clicobject = this;

        print("Castillo");

        selected = true;

        clickManager.GetComponent<ClickManager>().onclick += unclick;

    }
    //deselect the Entity
    public void unclick()
    {
        clickManager.GetComponent<ClickManager>().clicobject = null;
        selected = false;

    }
    public void da�ar(int dmg)
    {
        this.hp = this.hp - dmg;
        if (this.hp <= 0)
        {
            Destroy(this.gameObject);
        }
    }

}
