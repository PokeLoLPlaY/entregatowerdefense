using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TipoEnemigo
{
   Escorpion,
   Fantasmon,
   Calvo,
   MagoMalo,
   GiganteMalo,
   Caballero,
   Arquero,
   CaballeroMaza,
   Mago,
   Gigante
}
