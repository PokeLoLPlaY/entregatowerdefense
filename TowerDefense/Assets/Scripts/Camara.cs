using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{


    public int spd;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            print("w");
            transform.Translate(new Vector3( 0, spd * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.D))
        {
            print("d");
            transform.Translate(new Vector3(spd * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(new Vector3(-spd * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(new Vector3(0, -spd * Time.deltaTime, 0));
        }
        
    }
}
