using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

public class construccionBuilder : MonoBehaviour
{
    public TorreScriptable torreScriptable;

    public GameObject torre;

    public Cuartel cuartel;

    public GameObject[] recursosBuild;  // [0] - Almacen    // [1] - Farm

    public GameManager gameManager;

    public ClickManager clickManager;

    public GameObject tabGroup;

    public GameObject buildText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void comprarTorre()
    {
        //print("Coste de comida: " + torreScriptable.costeResources.comida);
        //print("Coste de oro: " + torreScriptable.costeResources.oro);
        //print("Coste de madera: " + torreScriptable.costeResources.madera);
        //print("Coste de piedra: " + torreScriptable.costeResources.piedra);

        if ((gameManager.comida >= torreScriptable.costeResources.comida) && (gameManager.oro >= torreScriptable.costeResources.oro) && (gameManager.madera >= torreScriptable.costeResources.madera) && (gameManager.piedra >= torreScriptable.costeResources.piedra))
        {
            gameManager.closeMainMenu();
            gameManager.gastarRecursos(torreScriptable.costeResources.comida, torreScriptable.costeResources.oro, torreScriptable.costeResources.madera, torreScriptable.costeResources.piedra);
            construirTorre();
            print("TORRE COMPRADA");
        }
        else
        {
            print("NO TIENES RECURSOS");
        }

    }

    public void construirTorre()
    {
        GameObject newTorre = Instantiate(torre);
        newTorre.GetComponent<NewTorre>().datos = torreScriptable;
        newTorre.GetComponent<dragObject>().buildText = buildText;
        newTorre.GetComponent<NewTorre>().GetComponent<SpriteRenderer>().sprite = torreScriptable.sprite;
        //HACER QUE APAREZACA EN EL CENTRO
        Vector2 lookAtPosition = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, Screen.height / 2));
        newTorre.transform.position = new Vector2(lookAtPosition.x, lookAtPosition.y);
        print("La torre ya esta en el mapa lista para construir");
    }

    public void comprarCuartel()
    {
        if ((gameManager.madera >= cuartel.GetComponent<All>().coste.comida) && (gameManager.madera >= cuartel.GetComponent<All>().coste.oro) && (gameManager.madera >= cuartel.GetComponent<All>().coste.madera) && (gameManager.madera >= cuartel.GetComponent<All>().coste.piedra))
        {
            gameManager.closeMainMenu();
            gameManager.gastarRecursos(cuartel.GetComponent<All>().coste.comida, cuartel.GetComponent<All>().coste.oro, cuartel.GetComponent<All>().coste.madera, cuartel.GetComponent<All>().coste.piedra);
            construirCuartel();
            print("CUARTEL COMPRADO");
        }
        else
        {
            print("NO TIENES RECURSOS");
        }
    }

    public void construirCuartel()
    {
        Cuartel newCuartel = Instantiate(cuartel);
        newCuartel.GetComponent<dragObject>().buildText = buildText;
        newCuartel.transform.name = "Cuartel";
        //HACER QUE APAREZACA EN EL CENTRO
        //Vector2 lookAtPosition = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, Screen.height / 2));
        //newCuartel.transform.position = new Vector2(lookAtPosition.x, lookAtPosition.y);
        newCuartel.transform.position = new Vector2(0, 0);
        print("El cuartel ya esta en el mapa listo para construir");
    }

    public void comprarRecursoBuild()
    {
        int index = -1;

        if (tabGroup.GetComponent<TabGroup>().getCosa() == "almacen")
        {
            index = 0;
            
        } else if (tabGroup.GetComponent<TabGroup>().getCosa() == "farm") {
            index = 1;
        }

        Coste coste = recursosBuild[index].GetComponent<All>().coste;

        if ((gameManager.comida >= coste.comida) && (gameManager.oro >= coste.oro) && (gameManager.madera >= coste.madera) && (gameManager.piedra >= coste.piedra))
        {
            gameManager.closeMainMenu();
            gameManager.gastarRecursos(coste.comida, coste.oro, coste.madera, coste.piedra);
            construirRecurso(tabGroup.GetComponent<TabGroup>().getCosa());
            print("RECURSO BUILDING COMPRADO");
        }
        else
        {
            print("NO TIENES RECURSOS");
        }

    }

    private void construirRecurso(String cosa)
    {
        if (cosa == "almacen")
        {
            GameObject newAlmacen = Instantiate(recursosBuild[0]);
            newAlmacen.GetComponent<dragObject>().buildText = buildText;
            //HACER QUE APAREZACA EN EL CENTRO
            Vector2 lookAtPosition = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, Screen.height / 2));
            newAlmacen.transform.position = new Vector2(lookAtPosition.x, lookAtPosition.y);
            newAlmacen.transform.name = "Almacen";
            //newAlmacen.transform.position = new Vector2(0, 0);
            //print("El recurso ya esta en el mapa listo para construir");

            //AGREGAR ALMACEN A LISTA DE GAME MANAGER
            List<Almacen> almacenes = (clickManager.GetComponent<ClickManager>().Almacenes).ToList();
            almacenes.Add(newAlmacen.GetComponent<Almacen>());
            clickManager.GetComponent<ClickManager>().Almacenes = almacenes.ToArray();
            Almacen[] almacenArray = almacenes.ToArray();

            for (int i = 0;i < almacenArray.Length;i++)
            {
                print(i + ": " + almacenArray[i]);
            }

            

            //print("PUM ALMACEN");
        } else if (cosa == "farm")
        {
            GameObject newFarm = Instantiate(recursosBuild[1]);
            newFarm.GetComponent<dragObject>().buildText = buildText;
            //HACER QUE APAREZACA EN EL CENTRO
            Vector2 lookAtPosition = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, Screen.height / 2));
            newFarm.transform.position = new Vector2(lookAtPosition.x, lookAtPosition.y);
            newFarm.transform.name = "Farm";
            //newFarm.transform.position = new Vector2(0, 0);
            //print("El recurso ya esta en el mapa listo para construir");

            //print("PUM FARM");
        }
    }
}
