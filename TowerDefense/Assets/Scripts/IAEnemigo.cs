using Pathfinding;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAEnemigo : MonoBehaviour
{
    IAstarAI ai;
    public float speed;
    public GameObject focusmov;
    public bool mov;
    public bool ontarget;

    private void OnEnable()
    {
        this.speed = this.GetComponent<Unidad>().spd;
        ai = GetComponent<IAstarAI>();
        if (ai != null) ai.onSearchPath += Update;
    }

    private void OnDisable()
    {
        if (ai != null) ai.onSearchPath -= Update;
        
    }

    // Start is called before the first frame update
    void Start()
    {
        this.speed = this.GetComponent<Unidad>().spd;
        ai.maxSpeed = speed;
        if (ai != null)
        {
            ai.destination = new Vector3(0, 0, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Unidad>().focus != null)
        {
            ontarget = true;
        }
        else
        {
            ontarget = false;
        }

        if (!ontarget)
        {
            ai.isStopped = false;
            if (this.GetComponent<Unidad>().focusmov != null)
            {
                this.focusmov = this.GetComponent<Unidad>().focusmov;
            }

            if (this.focusmov != null)
            {
                if (ai != null) ai.destination = this.focusmov.transform.position;
                this.mov = true;
            }
            else
            {
                if (ai != null) ai.destination = new Vector3(0, 0, 0);
                this.mov = true;
            }
 
        }
        else if (ontarget)
        {
            ai.isStopped = true;
            this.mov = false;
        }
        
    }
}
