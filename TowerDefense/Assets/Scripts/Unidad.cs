using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class Unidad : MonoBehaviour
{
    private new SpriteRenderer renderer;
    private Animator animator;
    private Rigidbody2D rb;
    public int hpmax;
    private int hp;
    public int atk;
    public float atkspd;
    public float spd;
    public GameObject focus;
    public GameObject focusmov;
    public GameObject pool;
    public TipoUnidad tipo;
    public TipoEnemigo tipo2;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(pegar());
        this.rb = this.GetComponent<Rigidbody2D>();
        this.animator = this.GetComponent<Animator>();
        this.renderer = this.GetComponent<SpriteRenderer>();
        //this.spd = this.GetComponent<Mov>().speed;
        this.hp = hpmax;
        if (this.tipo2 == TipoEnemigo.Fantasmon || this.tipo2 == TipoEnemigo.Arquero)
        {
            this.pool = GameObject.Find("PoolArqueroGenerica");
        }else if (this.tipo2 == TipoEnemigo.Mago || this.tipo2 == TipoEnemigo.MagoMalo)
        {
            this.pool = GameObject.Find("PoolMagoGenerica");
        }
        else
        {
            this.pool = GameObject.Find("PoolMeleeGenerica");
        }

    }

    // Update is called once per frame
    void Update()
    {
        idleorrun();
        
        if (this.focus != null)
        {
            if (this.transform.position.x-focus.transform.position.x > 0)
            {
                this.renderer.flipX = true;
            }else if (this.transform.position.x-focus.transform.position.x < 0)
            {
                this.renderer.flipX = false;
            }
        }

    }

    public void idleorrun()
    {
        if (this.GetComponent<Mov>() != null)
        {
            if (this.GetComponent<Mov>().mov)
            {
                this.animator.SetFloat("X", 1);
            }
            else
            {
                this.animator.SetFloat("X", 0);
            }
        }else if (this.GetComponent<IAEnemigo>() != null)
        {
            if (this.GetComponent<IAEnemigo>().mov)
            {
                this.animator.SetFloat("X", 1);
            }
            else
            {
                this.animator.SetFloat("X", 0);
            }
        }
        else
        {
            this.animator.SetFloat("X", 0);
        }
        
        /*
        if (this.x > this.transform.position.x)
        {
            renderer.flipX = true;
        }
        else if (this.x < this.transform.position.x)
        {
            renderer.flipX = false;
        }
        this.x = this.transform.position.x;
        */
    }

    public void da�ar(int dmg)
    {
        this.hp = this.hp - dmg;
        //print(this.name+": "+this.hp);
        if (this.hp <= 0)
        {
            this.morir();
        }
    }

    public void morir()
    {
        Destroy(this.gameObject);
        if (this.tipo == TipoUnidad.Enemigo)
        {
            GameObject spawner = GameObject.Find("SpawnerWaves");
            spawner.GetComponent<SpawnerWaves>().vivos.Remove(this.gameObject);
        }
    }
    
    IEnumerator pegar()
    {
        while (true)
        {
            yield return new WaitForSeconds(this.atkspd);

            if (this.focus != null)
            {

                for (int i = 0; i < this.pool.transform.childCount; i++)
                {
                    if (!this.pool.transform.GetChild(i).gameObject.activeSelf)
                    {
                        if (this.tipo == TipoUnidad.Aliado)
                        {
                            this.pool.transform.GetChild(i).GetComponent<Disparo>().tipo = TipoUnidad.Aliado;
                        }else if (this.tipo == TipoUnidad.Enemigo)
                        {
                            this.pool.transform.GetChild(i).GetComponent<Disparo>().tipo = TipoUnidad.Enemigo;
                        }
                        this.pool.transform.GetChild(i).GetComponent<Disparo>().focus = this.focus;
                        this.pool.transform.GetChild(i).GetComponent<Disparo>().dmg = this.atk;
                        this.pool.transform.GetChild(i).gameObject.SetActive(true);
                        this.pool.transform.GetChild(i).position = this.transform.position;
                        break;
                    }
                }

            }

        }

    }
    
}
