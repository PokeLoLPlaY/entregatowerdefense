using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class InfoRecursoManager : MonoBehaviour
{
    //public GameObject infoT;

    public GameObject[] recursosBuild;  // [0] - Almacen    // [1] - Farm

    private CanvasRenderer[] allchildren;

    private CanvasRenderer nombre;
    private CanvasRenderer sprite;
    private CanvasRenderer info;
    private CanvasRenderer coste;
    private CanvasRenderer boton;
    private CanvasRenderer costoContainer;

    public Sprite spriteComida;
    public Sprite spriteOro;
    public Sprite spriteMadera;
    public Sprite spritePiedra;

    public GameObject materialTemplate;

    private void Awake()
    {
        allchildren = this.transform.GetComponentsInChildren<CanvasRenderer>();

        nombre = allchildren[5];

        sprite = allchildren[7];

        info = allchildren[15];

        coste = allchildren[20];

        boton = allchildren[17];

        costoContainer = allchildren[19];
    }

    void Start()
    {

        //PARA SABER �NDICE DE CADA HIJO
        /*
        for (int i = 0;i < allchildren.Length;i++)
        {
            print("Hijo " + i + ", Nombre: " + allchildren[i].transform.name);
        }
        */
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void loadInfo(String cosa)
    {
        if (allchildren.Length > 0)
        {
            int index = -1;

            String infoRecursoManager = "";

            if (cosa == "almacen")
            {
                index = 0;

                infoRecursoManager = "Todo reino proteje sus bienes, usa el <b>Almacen</b> para guardar todos los recursos que vayas consiguiendo durante tu aventura.";

            }
            else if (cosa == "farm")
            {
                
                index = 1;

                infoRecursoManager = "La <b>Granja</b> te proveer� los alimentos necesarios para expandir tu reino i mantenerlo, �s un elemento indispensable.";

            }

            print("index: " + index);
            ponerRecursos(index);

            nombre.GetComponent<TextMeshProUGUI>().text = cosa;
            sprite.GetComponent<Image>().sprite = recursosBuild[index].GetComponent<SpriteRenderer>().sprite;
            info.GetComponent<TextMeshProUGUI>().text = infoRecursoManager;            


        }
    }

    public void ponerRecursos(int index)
    {
        
        CanvasRenderer[] materials = costoContainer.transform.GetComponentsInChildren<CanvasRenderer>();


        foreach (Transform child in costoContainer.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        int cont = 0;

        Coste coste = recursosBuild[index].GetComponent<All>().coste;

        print("costo comida: " + coste.comida.ToString());
        print("costo oro: " + coste.oro.ToString());
        print("costo madera: " + coste.madera.ToString());
        print("costo piedra: " + coste.piedra.ToString());



        if (coste.comida != 0)
        {
            CanvasRenderer material = Instantiate(materialTemplate.GetComponent<CanvasRenderer>());
            material.transform.SetParent(costoContainer.transform);
            material.transform.localScale = new Vector3(1, 1, 1);

            GameObject costo = material.transform.GetChild(0).gameObject;
            GameObject icon = material.transform.GetChild(1).gameObject;

            costo.GetComponent<TextMeshProUGUI>().text = coste.comida.ToString();
            icon.GetComponent<Image>().sprite = spriteComida;

            cont++;
        }
        if (coste.oro != 0)
        {
            CanvasRenderer material = Instantiate(materialTemplate.GetComponent<CanvasRenderer>());
            material.transform.SetParent(costoContainer.transform);
            material.transform.localScale = new Vector3(1, 1, 1);

            GameObject costo = material.transform.GetChild(0).gameObject;
            GameObject icon = material.transform.GetChild(1).gameObject;

            costo.GetComponent<TextMeshProUGUI>().text = coste.oro.ToString();
            icon.GetComponent<Image>().sprite = spriteOro;

            cont++;
        }
        if (coste.madera != 0)
        {
            CanvasRenderer material = Instantiate(materialTemplate.GetComponent<CanvasRenderer>());
            material.transform.SetParent(costoContainer.transform);
            material.transform.localScale = new Vector3(1, 1, 1);

            GameObject costo = material.transform.GetChild(0).gameObject;
            GameObject icon = material.transform.GetChild(1).gameObject;

            costo.GetComponent<TextMeshProUGUI>().text = coste.madera.ToString();
            icon.GetComponent<Image>().sprite = spriteMadera;

            cont++;
        }
        if (coste.piedra != 0)
        {
            CanvasRenderer material = Instantiate(materialTemplate.GetComponent<CanvasRenderer>());
            material.transform.SetParent(costoContainer.transform);
            material.transform.localScale = new Vector3(1, 1, 1);

            GameObject costo = material.transform.GetChild(0).gameObject;
            GameObject icon = material.transform.GetChild(1).gameObject;

            costo.GetComponent<TextMeshProUGUI>().text = coste.piedra.ToString();
            icon.GetComponent<Image>().sprite = spritePiedra;

            cont++;
        }

        int newPosX = 0;
        int newWidth = 100;

        if (cont == 1)
        {
            newPosX = 31;
            newWidth = 147;
        }
        else if (cont == 2)
        {
            newPosX = 37;
            newWidth = 290;
        }
        
        costoContainer.GetComponent<RectTransform>().localPosition = new Vector3(newPosX - 50, -47, 0);
        costoContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(newWidth, costoContainer.GetComponent<RectTransform>().sizeDelta.y);
        
    }


}
