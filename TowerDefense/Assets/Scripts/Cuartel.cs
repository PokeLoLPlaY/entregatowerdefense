using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Cuartel : MonoBehaviour
{
    public GameObject Caballero;
    public GameObject Arquero;
    public GameObject Caballeromaza;
    public GameObject Mago;
    public GameObject Gigante;

    public GameManager gm;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void crearunidad(int i)
    {
        switch (i)
        {
            case 0:
                if (gm.comida >= 50 && gm.oro>=25)
                {
                    print("Hola");
                    GameObject newspawn = Instantiate((GameObject)Caballero);
                    newspawn.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    gm.comida -= 50;
                    gm.oro -= 25;
                }
                break;
            case 1:
                if (gm.comida >= 40 && gm.madera >= 25)
                {
                    print("Hola");
                    GameObject newspawn = Instantiate((GameObject)Arquero);
                    newspawn.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    gm.comida -= 40;
                    gm.madera -= 25;
                }
                break;
            case 2:
                if (gm.comida >= 60 && gm.oro >= 50)
                {
                    print("Hola");
                    GameObject newspawn = Instantiate((GameObject)Caballeromaza);
                    newspawn.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    gm.comida -= 60;
                    gm.oro -= 50;
                }
                break;
            case 3:
                if (gm.comida >= 30 && gm.oro >= 80)
                {
                    print("Hola");
                    GameObject newspawn = Instantiate((GameObject)Mago);
                    newspawn.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    gm.comida -= 30;
                    gm.oro -= 80;
                }
                break;
            case 4:
                if (gm.comida >= 100 && gm.oro >=100)
                {
                    print("Hola");
                    GameObject newspawn = Instantiate((GameObject)Gigante);
                    newspawn.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                    gm.comida -= 100;
                    gm.oro -= 100;
                }
                break;
            
        }
    }
}
