using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu]
public class Recursos : ScriptableObject
{
    // Start is called before the first frame update
    public int nresources;

    public Sprite sprite;

    public TipoResource resource;
}
