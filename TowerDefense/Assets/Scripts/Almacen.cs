using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Almacen : MonoBehaviour
{
    public GameManager gm;

    public int hp;
    public float x;
    public float y;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        x = this.gameObject.transform.position.x;
        y = this.gameObject.transform.position.y;
    }
    public void recoger(GameObject g)
    {
        switch (g.GetComponent<Aldaeno>().resourceType) {
            case TipoResource.Wood:
                gm.madera += g.GetComponent<Aldaeno>().nrec;
                break;
            case TipoResource.Food:
                gm.comida += g.GetComponent<Aldaeno>().nrec;
                break;
            case TipoResource.Stone:
                gm.piedra += g.GetComponent<Aldaeno>().nrec;
                break;
            case TipoResource.Gold:
                gm.oro += g.GetComponent<Aldaeno>().nrec;
                break;
            default:
                print("No tienes nah");
                break;
        }




        g.GetComponent<Aldaeno>().nrec = 0;

        g.GetComponent<Aldaeno>().resourceType = TipoResource.Void;

    }

    public void da�ar(int dmg)
    {
        this.hp = this.hp - dmg;
        if (this.hp <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
