using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class TabGroup : MonoBehaviour
{
    public List<TabButton> tabButtons;
    public Sprite tabIdle;
    public Sprite tabHover;
    public Sprite tabActive;
    public TabButton selectedTab;
    public GameObject objectToSwap;

    public List<TorreScriptable> torresScriptables;

    public GameObject mainMenu;
    public GameObject infoMenu;
    public GameObject infoTemplate;

    private string cosa;

    public string getCosa()
    {
        return this.cosa;
    }
    public void setCosa(String cosa)
    {
        this.cosa = cosa;
    }

    public void Start()
    {
        ResetTabs();
    }
    public void Subscribe(TabButton button)
    {
        if (tabButtons == null)
        {
            tabButtons = new List<TabButton>();
        }

        tabButtons.Add(button);
    }

    public void OnTabEnter(TabButton button)
    {
        ResetTabs();
        if (selectedTab == null || button != selectedTab)
        {
            button.background.sprite = tabHover;
        }
    }

    public void OnTabExit(TabButton button)
    {
        ResetTabs();
    }

    public void OnTabSelected(TabButton button)
    {
        //selectedTab = button;
        ResetTabs();
        HideMainMenu();
        //button.background.sprite = tabActive;

        
        if (cosa == "nada")
        {
            int index = button.transform.GetSiblingIndex();
            for (int i = 0; i < torresScriptables.Count; i++)
            {
                if (i == index)
                {
                    infoTemplate.GetComponent<InfoTorreManager>().torreScriptable = torresScriptables[i];
                    infoTemplate.GetComponent<InfoTorreManager>().loadInfo();
                }

            }

            infoTemplate.GetComponent<InfoCuartelManager>().loadInfo();

        } else
        {
            //print("TabGroup: " + this.cosa);
            infoTemplate.GetComponent<InfoRecursoManager>().loadInfo(this.cosa);
        }
    }

    private void HideMainMenu()
    {
        mainMenu.SetActive(false);
        infoMenu.SetActive(true);
    }

    public void ResetTabs()
    {
        foreach(TabButton button in tabButtons)
        {

            if (selectedTab!=null && button == selectedTab)
            {
                continue;
            }
            button.background.sprite = tabIdle;
        }
    }
}
