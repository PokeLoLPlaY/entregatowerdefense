using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TabButton : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler  
{

    public TabGroup tabGroup;

    public Image background;

    public string cosa = "nada";

    public void OnPointerClick(PointerEventData eventData)
    {
        tabGroup.setCosa(this.cosa);
        tabGroup.OnTabSelected(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tabGroup.OnTabEnter(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tabGroup.OnTabExit(this);
    }

    void Start()
    {
        tabGroup.Subscribe(this);
        background = GetComponent<Image>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
