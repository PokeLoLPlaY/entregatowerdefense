using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class knightMechanics : MonoBehaviour
{
    private float spd = 3;
    private Rigidbody2D knight;
    // Start is called before the first frame update
    void Start()
    {
        knight = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            knight.velocity = new Vector2(-spd, knight.velocity.y);
            print("A");
        } else if (Input.GetKey(KeyCode.D))
        {
            knight.velocity = new Vector2(+spd, knight.velocity.y);
            print("D");
        } else
        {
            knight.velocity = new Vector2(0, knight.velocity.y);
        }

        if (Input.GetKey(KeyCode.W))
        {
            knight.velocity = new Vector2(knight.velocity.x, +spd);
            print("W");
        }
        else if (Input.GetKey(KeyCode.S))
        {
            knight.velocity = new Vector2(knight.velocity.x, -spd);
            print("S");
        } else
        {
            knight.velocity = new Vector2(knight.velocity.x, 0);
        }

    }

    public void OnMouseDown()
    {
        
    }
}
