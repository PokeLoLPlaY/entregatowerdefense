using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Edificios : ScriptableObject
{
    // Start is called before the first frame update
    public int health;

    public Sprite sprite;

    public TipoEdf edf;

    public TipoGlobal tg;
}
