using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoEnemigos : MonoBehaviour
{
    public Queue<GameObject> cola = new Queue<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (cola.Count != 0)
        {
            GetComponentInParent<Unidad>().focus = cola.Peek();
        }
        else
        {
            GetComponentInParent<Unidad>().focus = null;
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Aliados" || collision.transform.tag == "Almacen" || collision.transform.tag == "Cuartel")
        {
            cola.Enqueue(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Aliados" || collision.transform.tag == "Almacen" || collision.transform.tag == "Cuartel")
        {
            Queue<GameObject> cola2 = new Queue<GameObject>();

            foreach (GameObject obj in cola)
            {
                if (!obj.Equals(collision.gameObject))
                {
                    cola2.Enqueue(obj);
                }
            }

            cola = cola2;

            GetComponentInParent<Unidad>().focus = null;

        }
    }

}
