using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoVisionEnemigo : MonoBehaviour
{
    private TipoUnidad tipo;
    public Queue<GameObject> cola = new Queue<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        this.tipo = GetComponentInParent<Unidad>().tipo;
    }

    // Update is called once per frame
    void Update()
    {
        if (cola.Count != 0)
        {
            GetComponentInParent<Unidad>().focusmov = cola.Peek();
        }
        else
        {
            GetComponentInParent<Unidad>().focusmov = null;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (this.tipo == TipoUnidad.Enemigo)
        {
            if (collision.transform.tag == "Aliados" || collision.transform.tag == "Almacen" || collision.transform.tag == "Cuartel") 
            {
                cola.Enqueue(collision.gameObject);
            }
        }else if (this.tipo == TipoUnidad.Aliado)
        {
            if (collision.transform.tag == "Enemigos")
            {
                cola.Enqueue(collision.gameObject);
            }
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (this.tipo == TipoUnidad.Enemigo)
        {
            if (collision.transform.tag == "Aliados" || collision.transform.tag == "Almacen" || collision.transform.tag == "Cuartel")
            {
                Queue<GameObject> cola2 = new Queue<GameObject>();

                foreach (GameObject obj in cola)
                {
                    if (!obj.Equals(collision.gameObject))
                    {
                        cola2.Enqueue(obj);
                    }
                }

                cola = cola2;

                GetComponentInParent<Unidad>().focusmov = null;

            }
        }else if (this.tipo == TipoUnidad.Aliado)
        {
            if (collision.transform.tag == "Enemigos")
            {
                Queue<GameObject> cola2 = new Queue<GameObject>();

                foreach (GameObject obj in cola)
                {
                    if (!obj.Equals(collision.gameObject))
                    {
                        cola2.Enqueue(obj);
                    }
                }

                cola = cola2;

                GetComponentInParent<Unidad>().focusmov = null;

            }
        }
        
    }
   
    private void OnMouseDown()
    {
        print("Hola");
    }

}
