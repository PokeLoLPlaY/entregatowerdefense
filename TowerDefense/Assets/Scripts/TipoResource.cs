using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TipoResource
{
    // Start is called before the first frame update
    Wood, Gold, Food, Stone, Void
}
