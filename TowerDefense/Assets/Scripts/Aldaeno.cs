using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Aldaeno : MonoBehaviour
{
    private Animator animator;
    public bool selected = false;
    public float speed = 10f;
    public bool mov=false;
    ClickManager clickManager;
    Vector2 lastclickedpos;
    public TipoResource resourceType;
    public int nrec = 0;


    public Almacen[] Almacenes;

    public Almacen almacen;
    public bool recogiendo = false;
    public GameObject target;
    // Start is called before the first frame update
    void Start()
    {
        clickManager = FindObjectOfType<ClickManager>();
        this.animator = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        idleorrun();
        this.Almacenes = clickManager.Almacenes;
        //Slave go to the warehouse
        if (this.nrec == 15 && this.resourceType!=TipoResource.Void)
        {
            for (int i = 0; i < this.Almacenes.Length; i++)
            {
                if (this.almacen == null&& Almacenes[i] != null)
                {
                    this.almacen = Almacenes[i];
                }
                else
                {
                    if (Almacenes[i] != null) 
                    {
                        Double a = Math.Sqrt(Math.Pow(almacen.x - this.gameObject.transform.position.x, 2) + Math.Pow(almacen.y - this.gameObject.transform.position.y, 2));
                        Double a2 = Math.Sqrt(Math.Pow(Almacenes[i].x - this.gameObject.transform.position.x, 2) + Math.Pow(Almacenes[i].y - this.gameObject.transform.position.y, 2));
                        if (a2 < a)
                        {
                            almacen = Almacenes[i];
                        }
                    }
                    
                }
            }
            Vector2 posalm = new Vector2(almacen.transform.position.x, almacen.transform.position.y);
            if ((Vector2)transform.position != posalm)
            {
                this.mov = true;
                float step = speed * Time.deltaTime;
                transform.position = Vector2.MoveTowards(transform.position, posalm, step);

            }
            recogiendo = false;
        }
        //Slave go to the targeted resource
        else if (target != null && !recogiendo && this.nrec<1) 
        {
           

            Vector2 posresource = new Vector2(target.transform.position.x, target.transform.position.y);

            mov = true;

            float step = speed * Time.deltaTime;

            transform.position = Vector2.MoveTowards(transform.position, posresource, step);

            
        }
        //Slave whereever you want
        else
        {
            //Select the spected position
            if (selected && Input.GetMouseButtonDown(1))
            {
                this.target = null;
                lastclickedpos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mov = true;
            }
            //Move to the seleted position
            if (mov && (Vector2)transform.position != lastclickedpos)
            {
                float step = speed * Time.deltaTime;
                transform.position = Vector2.MoveTowards(transform.position, lastclickedpos, step);
            }
            //Stop once he get the selected position
            else
            {
                mov = false;
            }
        }

        if (lastclickedpos != null && mov)
        {
            if (lastclickedpos.x < this.transform.position.x)
            {
                this.GetComponent<SpriteRenderer>().flipX = true;
            }
            else if (lastclickedpos.x > this.transform.position.x)
            {
                this.GetComponent<SpriteRenderer>().flipX = false;
            }
        }

    }

    public void idleorrun()
    {

        if (this.mov && !this.recogiendo)
        {
            this.animator.SetFloat("X", 1);
        }
        else
        {
            this.animator.SetFloat("X", 0);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
       

        

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        print(collision.transform.name);
        if (collision.transform.tag.Equals("Granja") && this.nrec < 15)
        {
            
            if (this.resourceType!=TipoResource.Food) 
            { 

                this.resourceType = TipoResource.Food;

                
            }
            
            this.target = collision.gameObject;
        }
        if(!collision.transform.tag.Equals("Granja"))
        {
            if (collision.transform.parent.tag.Equals("Arbol") && this.nrec < 15)
            {
                this.target = collision.gameObject;
                this.mov = false;
                
                
            }

            if (collision.transform.parent.tag.Equals("Almacen"))
            {
                this.mov = false;
                if (this.nrec != 0)
                {
                   

                    collision.transform.parent.GetComponent<Almacen>().recoger(this.gameObject);

                }
            }
        }
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
       
        if (!recogiendo)
        {
            if (collision.transform.tag.Equals("Granja") && this.nrec < 15)
            {

                recogiendo = true;

                StartCoroutine(recolectar(collision.gameObject));

            }
            else if(this.nrec<15)
            {
                if (collision.transform.parent.tag.Equals("Arbol"))
                {

                    recogiendo = true;

                    StartCoroutine(recolectar(collision.gameObject));

                }
            }
            

            

        }


        

    }
    
    
    //Select the Entity
    private void OnMouseDown()
    {

        

        clickManager.GetComponent<ClickManager>().clicked();

        clickManager.GetComponent<ClickManager>().clicobject = this;

        selected = true;

        clickManager.GetComponent<ClickManager>().onclick += unclick;
        


    }
    //deselect the Entity
    public void unclick()
    {

        selected=false;
        clickManager.clicobject = null;

    }
    //The slave pick up the resource
    private IEnumerator recolectar(GameObject g)
    {
        if (g.transform.tag == "Granja") {
            if (this.resourceType==g.GetComponent<Arbol>().tipo)
            {
                
                g.GetComponentInParent<Arbol>().Nresources--;

                this.nrec++;

                yield return new WaitForSeconds(1);

                recogiendo = false;
            }

            else
            {
                this.nrec = 0;
                this.resourceType=g.GetComponent<Arbol>().tipo;
               

                g.GetComponentInParent<Arbol>().Nresources--;

                this.nrec++;
                yield return new WaitForSeconds(1);
                recogiendo = false;

            }
        }
        else {
            if (this.resourceType==g.GetComponentInParent<Arbol>().tipo)
            {
                

                g.GetComponentInParent<Arbol>().Nresources--;

                this.nrec++;
                yield return new WaitForSeconds(1);
                recogiendo = false;
            }

            else
            {
                this.nrec = 0;
                
                this.resourceType=g.GetComponentInParent<Arbol>().tipo;
                g.GetComponentInParent<Arbol>().Nresources--;

                this.nrec++;
                yield return new WaitForSeconds(1);
                recogiendo = false;

            }
        }
        
       
    }
    private IEnumerator stock(GameObject g)
    {
        yield return new WaitForSeconds(1);

    }

}
