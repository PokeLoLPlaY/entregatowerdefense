using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dragObject : MonoBehaviour
{
    public bool builded = false;

    public GameObject buildText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!builded)
        {
            this.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, .8f);
            buildText.SetActive(true);
        } else
        {
            this.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 1f);
            buildText.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.C) && !builded)
        {
            builded = true;
        }
    }

    public void OnMouseDown()
    {
        print("cosa");
    }

    public void OnMouseDrag()
    {
        if (!builded)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.transform.position = new Vector2(pos.x, pos.y);
            print("DRAGGGGG");
        } else
        {
            print("La torre ya esta construida");
        }
    }
}
