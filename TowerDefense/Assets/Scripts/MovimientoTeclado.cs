using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoTeclado : MonoBehaviour
{
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        this.rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector2(4, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector2(-4, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.W)) 
        {
            rb.velocity = new Vector2(rb.velocity.x, 4);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.velocity = new Vector2(rb.velocity.x, -4);
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }
    }
}
