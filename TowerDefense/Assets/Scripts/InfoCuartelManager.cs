using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class InfoCuartelManager : MonoBehaviour
{
    //public GameObject infoT;

    public Cuartel cuartelInfo;

    private CanvasRenderer[] allchildren;

    private CanvasRenderer nombre;
    private CanvasRenderer sprite;
    private CanvasRenderer info;
    private CanvasRenderer coste;
    private CanvasRenderer boton;
    private CanvasRenderer costoContainer;

    public Sprite spriteComida;
    public Sprite spriteOro;
    public Sprite spriteMadera;
    public Sprite spritePiedra;

    public GameObject materialTemplate;

    private void Awake()
    {
        allchildren = this.transform.GetComponentsInChildren<CanvasRenderer>();

        nombre = allchildren[5];

        sprite = allchildren[7];

        info = allchildren[15];

        boton = allchildren[17];

        coste = allchildren[21];
    }

    void Start()
    {

        //PARA SABER �NDICE DE CADA HIJO
        /*
        for (int i = 0;i < allchildren.Length;i++)
        {
            print("Hijo " + i + ", Nombre: " + allchildren[i].transform.name);
        }
        */
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void loadInfo()
    {
        if (allchildren.Length > 0)
        {
            nombre.GetComponent<TextMeshProUGUI>().text = "Cuartel";
            //sprite.GetComponent<Image>().sprite = cuartelInfo.GetComponent<SpriteRenderer>().sprite;
            info.GetComponent<TextMeshProUGUI>().text = "Cuartel principal de tu reino, haz que tus tropas salgan de aqu� cuando quieras, pero cuidado, necesitar�s oro...";
            //coste.GetComponent<TextMeshProUGUI>().text = torreScriptable.coste.ToString();

            //print("hijo: " + costoContainer.transform.GetChild(0).name);

            CanvasRenderer[] materials = costoContainer.transform.GetComponentsInChildren<CanvasRenderer>();
            /*
            for (int i = 1;i < materials.Length;i+=4)
            {
                print("i: " + i + ", hijo: " + materials[i].transform.name);
                Destroy(costoContainer.transform.GetChild(i).gameObject);
            }*/

            foreach (Transform child in costoContainer.transform)
            {
                GameObject.Destroy(child.gameObject);
            }

            //print("costo comida: " + cuartelInfo.GetComponent<All>().coste.comida.ToString());
            //print("costo oro: " + cuartelInfo.GetComponent<All>().coste.oro.ToString());
            //print("costo madera: " + cuartelInfo.GetComponent<All>().coste.madera.ToString());
            //print("costo piedra: " + cuartelInfo.GetComponent<All>().coste.piedra.ToString());

            int cont = 0;

            if (cuartelInfo.GetComponent<All>().coste.comida != 0)
            {
                CanvasRenderer material = Instantiate(materialTemplate.GetComponent<CanvasRenderer>());
                material.transform.SetParent(costoContainer.transform);
                material.transform.localScale = new Vector3(1, 1, 1);

                //print(material.transform.GetChild(0).gameObject);
                //print(material.transform.GetChild(1).gameObject);

                GameObject costo = material.transform.GetChild(0).gameObject;
                GameObject icon = material.transform.GetChild(1).gameObject;
                //print(costo.GetComponent<CanvasRenderer>().transform.name);
                //print(icon.GetComponent<CanvasRenderer>().transform.name);

                costo.GetComponent<TextMeshProUGUI>().text = cuartelInfo.GetComponent<All>().coste.comida.ToString();
                icon.GetComponent<Image>().sprite = spriteComida;

                cont++;
            }
            if (cuartelInfo.GetComponent<All>().coste.oro != 0)
            {
                CanvasRenderer material = Instantiate(materialTemplate.GetComponent<CanvasRenderer>());
                material.transform.SetParent(costoContainer.transform);
                material.transform.localScale = new Vector3(1, 1, 1);

                //print(material.transform.GetChild(0).gameObject);
                //print(material.transform.GetChild(1).gameObject);

                GameObject costo = material.transform.GetChild(0).gameObject;
                GameObject icon = material.transform.GetChild(1).gameObject;
                //print(costo.GetComponent<CanvasRenderer>().transform.name);
                //print(icon.GetComponent<CanvasRenderer>().transform.name);

                costo.GetComponent<TextMeshProUGUI>().text = cuartelInfo.GetComponent<All>().coste.oro.ToString();
                icon.GetComponent<Image>().sprite = spriteOro;

                cont++;
            }
            if (cuartelInfo.GetComponent<All>().coste.madera != 0)
            {
                CanvasRenderer material = Instantiate(materialTemplate.GetComponent<CanvasRenderer>());
                material.transform.SetParent(costoContainer.transform);
                material.transform.localScale = new Vector3(1, 1, 1);

                //print(material.transform.GetChild(0).gameObject);
                //print(material.transform.GetChild(1).gameObject);

                GameObject costo = material.transform.GetChild(0).gameObject;
                GameObject icon = material.transform.GetChild(1).gameObject;
                //print(costo.GetComponent<CanvasRenderer>().transform.name);
                //print(icon.GetComponent<CanvasRenderer>().transform.name);

                costo.GetComponent<TextMeshProUGUI>().text = cuartelInfo.GetComponent<All>().coste.madera.ToString();
                icon.GetComponent<Image>().sprite = spriteMadera;

                cont++;
            }
            if (cuartelInfo.GetComponent<All>().coste.piedra != 0)
            {
                CanvasRenderer material = Instantiate(materialTemplate.GetComponent<CanvasRenderer>());
                material.transform.SetParent(costoContainer.transform);
                material.transform.localScale = new Vector3(1, 1, 1);

                //print(material.transform.GetChild(0).gameObject);
                //print(material.transform.GetChild(1).gameObject);

                GameObject costo = material.transform.GetChild(0).gameObject;
                GameObject icon = material.transform.GetChild(1).gameObject;
                //print(costo.GetComponent<CanvasRenderer>().transform.name);
                //print(icon.GetComponent<CanvasRenderer>().transform.name);

                costo.GetComponent<TextMeshProUGUI>().text = cuartelInfo.GetComponent<All>().coste.piedra.ToString();
                icon.GetComponent<Image>().sprite = spritePiedra;

                cont++;
            }

            int newPosX = 0;
            int newWidth = 100;

            if (cont == 1)
            {
                newPosX = 31;
                newWidth = 125;
            }
            else if (cont == 2)
            {
                newPosX = 36;
                newWidth = 250;
            }

            costoContainer.GetComponent<RectTransform>().localPosition = new Vector3(newPosX - 50, -47, 0);
            costoContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(newWidth, costoContainer.GetComponent<RectTransform>().sizeDelta.y);
        }
        

        print("INFO MANAGER LOADED WITH CUARTEL");
    }

    
}
