using Mono.Cecil.Cil;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class ClickCanvas : MonoBehaviour
{
    ClickManager clickManager;

    public CanvasRenderer[] allchildren;

    private void Awake()
    {
        allchildren = this.transform.GetComponentsInChildren<CanvasRenderer>();


       
    }
    // Start is called before the first frame update
    void Start()
    {
        clickManager = FindObjectOfType<ClickManager>();
    }

    // Update is called once per frame
    void Update()
    {

        allchildren[1].GetComponent<Image>().sprite = clickManager.clicobject.GetComponent<SpriteRenderer>().sprite;

        if (clickManager.clicobject != null)
        {
            //Activan and deactive the botons depending of the current clicked object
            if (clickManager.clicobject.GetComponent<All>().all == TipoGlobal.Construcccion)
            {

                if (clickManager.clicobject.GetComponent<All>().tipoedf == TipoEdf.Castillo)
                {

                    clickManager.bot[0].SetActive(true);
                    clickManager.bot[1].SetActive(false);
                    clickManager.bot[2].SetActive(false);
                    clickManager.bot[3].SetActive(false);
                    clickManager.bot[4].SetActive(false);
                    clickManager.bot[5].SetActive(false);

                    CanvasRenderer[] Imagen = clickManager.bot[0].GetComponentsInChildren<CanvasRenderer>();
                    Imagen[1].GetComponent<Image>().sprite = clickManager.clicobject.GetComponent<Castillo>().SpawnObject.GetComponent<SpriteRenderer>().sprite;
                }
                
                else if (clickManager.clicobject.GetComponent<All>().tipoedf == TipoEdf.Cuartel)
                {
                    clickManager.bot[0].SetActive(false);
                    clickManager.bot[1].SetActive(true);
                   
                    clickManager.bot[2].SetActive(true);
                    clickManager.bot[3].SetActive(true);
                    clickManager.bot[4].SetActive(true);
                    clickManager.bot[5].SetActive(true);

                    CanvasRenderer[] Imagen = clickManager.bot[1].GetComponentsInChildren<CanvasRenderer>();
                    Imagen[1].GetComponent<Image>().sprite = clickManager.clicobject.GetComponent<Cuartel>().Caballero.GetComponent<SpriteRenderer>().sprite;

                    CanvasRenderer[] Imagen2 = clickManager.bot[2].GetComponentsInChildren<CanvasRenderer>();
                    Imagen2[1].GetComponent<Image>().sprite = clickManager.clicobject.GetComponent<Cuartel>().Arquero.GetComponent<SpriteRenderer>().sprite;

                    CanvasRenderer[] Imagen3 = clickManager.bot[3].GetComponentsInChildren<CanvasRenderer>();
                    Imagen3[1].GetComponent<Image>().sprite = clickManager.clicobject.GetComponent<Cuartel>().Caballeromaza.GetComponent<SpriteRenderer>().sprite;

                    CanvasRenderer[] Imagen4 = clickManager.bot[4].GetComponentsInChildren<CanvasRenderer>();
                    Imagen4[1].GetComponent<Image>().sprite = clickManager.clicobject.GetComponent<Cuartel>().Mago.GetComponent<SpriteRenderer>().sprite;

                    CanvasRenderer[] Imagen5 = clickManager.bot[5].GetComponentsInChildren<CanvasRenderer>();
                    Imagen5[1].GetComponent<Image>().sprite = clickManager.clicobject.GetComponent<Cuartel>().Gigante.GetComponent<SpriteRenderer>().sprite;


                }
                

            }
            if (clickManager.clicobject.GetComponent<All>().all == TipoGlobal.Unidad) {
                clickManager.bot[0].SetActive(false);
                clickManager.bot[1].SetActive(false);
                clickManager.bot[2].SetActive(false);
                clickManager.bot[3].SetActive(false);
                clickManager.bot[4].SetActive(false);
                clickManager.bot[5].SetActive(false);

            }
        }

    }

}
