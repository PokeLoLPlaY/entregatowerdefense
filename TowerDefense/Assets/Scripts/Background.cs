using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static ClickManager;

public class Background : MonoBehaviour
{
    ClickManager clickManager;
    // Start is called before the first frame update
    void Start()
    {
        clickManager = FindObjectOfType<ClickManager>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnMouseDown()
    {
        clickManager.GetComponent<ClickManager>().clicked();

    }

}
