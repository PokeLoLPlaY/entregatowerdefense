
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ClickManager;

public class Arbol : MonoBehaviour

{
   
    public Recursos resource;
    public delegate void Contact();
    public event Contact OnContact;
    public int Nresources;
    public TipoResource tipo;
   

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<SpriteRenderer>().sprite = resource.sprite;
        this.Nresources = resource.nresources;
        this.tipo = resource.resource;
    }

    // Update is called once per frame
    void Update()
    {
        if (Nresources <=0) 
        {
            Destroy(this.gameObject);
        }
    }
    public void recolectar()
    {
        if (OnContact!=null) 
        {
            
            OnContact.Invoke();
            StartCoroutine(recolectado());
            
        }
        
        
    }
    public void eliminaraldeano()
    {
        OnContact = null;
    }
    private IEnumerator recolectado()
    {
        yield return new WaitForSeconds(1);
        Nresources--;
    }
}
